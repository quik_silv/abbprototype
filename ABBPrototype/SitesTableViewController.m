//
//  SitesTableViewController.m
//  ABBPrototype
//
//  Created by mark on 7/7/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "SitesTableViewController.h"
#import "CreateNewCaseTableViewController.h"
#import "SOTTableViewController.h"
@interface SitesTableViewController ()

@end

@implementation SitesTableViewController

NSArray *siteText;
//@synthesize delegate;
@synthesize selectedLocation;
@synthesize segueSource;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    siteText = [NSArray arrayWithObjects:@"ABB facility", @"Customer site", @"Project site", @"Commuting", @"Business travel", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [siteText count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sites" forIndexPath:indexPath];
    cell.textLabel.text = [siteText objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // you can see selected row number in your console;
    selectedLocation = [siteText objectAtIndex:indexPath.row];
    if([segueSource isEqual: @"SOT"]) {
        [self performSegueWithIdentifier:@"selected_site_sot" sender:self];
    } else {
        [self performSegueWithIdentifier:@"selected_location" sender:self];
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
//    if ([segue.identifier isEqualToString:@"selected_location"]) {
//        CreateNewCaseTableViewController *destViewController = segue.destinationViewController;
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        destViewController.site = [NSString stringWithFormat:@"%@", [siteText objectAtIndex:indexPath.row] ];
//    }
//    if ([segue.identifier isEqualToString:@"selected_site_sot"]) {
//        SOTTableViewController *destViewController = segue.destinationViewController;
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        destViewController.chosenSiteProject = [siteText objectAtIndex:indexPath.row];
//    }
//    if ([segue.identifier isEqualToString:@"selected_site"]) {
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        self.delegate = [segue destinationViewController];
//        [self.delegate addLocationTableViewController:self didFinishEnteringItem:[siteText objectAtIndex:indexPath.row] ];
//    }
//    [self.navigationController popViewControllerAnimated:YES];
}


@end
