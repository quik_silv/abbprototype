//
//  CoInspectorTableViewController.h
//  ABBPrototype
//
//  Created by Mark Wong on 9/5/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoInspectorTableViewController : UITableViewController
@property NSString * selectedCoInspector;
@end
