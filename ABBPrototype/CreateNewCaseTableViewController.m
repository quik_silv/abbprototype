//
//  CreateNewCaseTableViewController.m
//  ABBPrototype
//
//  Created by mark on 7/5/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "AppDelegate.h"
#import "RemoteJson.h"
#import "CasesTableViewController.h"
#import "CreateNewCaseTableViewController.h"
#import "TypesTableViewController.h"
#import "HazardReport.h"

@interface CreateNewCaseTableViewController ()

@end

@implementation CreateNewCaseTableViewController

@synthesize sourceVC;
@synthesize managedObject;
@synthesize employeeId;
@synthesize hazardType;
@synthesize type;
@synthesize site;
@synthesize location;
@synthesize hrDescription;
@synthesize image;
@synthesize action;

@synthesize hazardTypeLabel;
@synthesize siteLabel; //Labelled Location
@synthesize selectedPhoto;
@synthesize sendButtonLabel;
@synthesize siteTextField;
@synthesize descriptionTextView;
@synthesize actionTextView;
@synthesize chosenTypeLabel;
@synthesize chosenCategoryLabel;
@synthesize chosenLocation;
NSUserDefaults *formData;

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    formData = [NSUserDefaults standardUserDefaults];
    if ([sourceVC isKindOfClass:[CasesTableViewController class] ]) {
        [chosenCategoryLabel setText:type];
        [chosenTypeLabel setText:hazardType];
        [siteTextField setText:site];
        [chosenLocation setText:location];
        [descriptionTextView setText:hrDescription];
        [actionTextView setText:@""];

        [selectedPhoto setImage:[UIImage imageWithData:image] ];
    } else {
    
        // Uncomment the following line to preserve selection between presentations.
        // self.clearsSelectionOnViewWillAppear = NO;
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem;
        
        [chosenCategoryLabel setText:[formData objectForKey:@"hazardType"] ];
        [chosenLocation setText:[formData objectForKey:@"site_type"] ];
        [siteTextField setText:[formData objectForKey:@"site"] ];
        [chosenTypeLabel setText:[formData objectForKey:@"type"] ];
        [descriptionTextView setText:[formData objectForKey:@"description"] ];
        [actionTextView setText:[formData objectForKey:@"action"] ];
    }
    [siteTextField setDelegate:self];
    [descriptionTextView setDelegate:self];
    [actionTextView setDelegate:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 8;
}
*/
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([siteTextField isFirstResponder] && (siteTextField != touch.view)){
        [formData setValue:siteTextField.text forKey:@"site"];
    }
    if ([descriptionTextView isFirstResponder] && (descriptionTextView != touch.view)){
        [formData setValue:descriptionTextView.text forKey:@"description"];
        [descriptionTextView resignFirstResponder];
    }
    if ([actionTextView isFirstResponder] && (actionTextView != touch.view)){
        [formData setValue:actionTextView.text forKey:@"action"];
        [actionTextView resignFirstResponder];
    }
}
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
- (IBAction)saveData:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *savedData = [NSEntityDescription insertNewObjectForEntityForName:@"HazardReport" inManagedObjectContext:context];
    [savedData setValue:@"123456" forKey:@"employee_id"];
    [savedData setValue:[formData objectForKey:@"hazardType"] forKey:@"hazard_type"];
    [savedData setValue:[formData objectForKey:@"type"] forKey:@"type"];
    [savedData setValue:[formData objectForKey:@"site"] forKey:@"site"];
    [savedData setValue:[formData objectForKey:@"site_type"] forKey:@"site_type"];
    [savedData setValue:[formData objectForKey:@"description"] forKey:@"hrDescription"];
    [savedData setValue:@"open" forKey:@"status"];
    [savedData setValue:[formData objectForKey:@"action"] forKey:@"action"];
    [savedData setValue:[NSData dataWithData:UIImagePNGRepresentation([selectedPhoto image])] forKey:@"image"];

    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    } else {
        [formData removeObjectForKey:@"hazardType"];
        [formData removeObjectForKey:@"type"];
        [formData removeObjectForKey:@"site"];
        [formData removeObjectForKey:@"site_type"];
        [formData removeObjectForKey:@"description"];
        [formData removeObjectForKey:@"action"];
        [chosenCategoryLabel setText:@""];
        [chosenLocation setText:@""];
        [chosenTypeLabel setText:@""];
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Infomation"
                                                              message:@"Hazard report saved."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submit:(id)sender {
    [formData setValue:hazardTypeLabel.text forKey:@"hazardType"];
    [formData setValue:chosenCategoryLabel.text forKey:@"type"];
    [formData setValue:chosenLocation.text forKey:@"site_type"];
    [formData setValue:siteTextField.text forKey:@"site"];
    [formData setValue:descriptionTextView.text forKey:@"description"];
    [formData setValue:actionTextView.text forKey:@"action"];
    NSDictionary * data = @{
                            @"employee_id": @"123456",
                            @"hazard_type": [formData objectForKey:@"hazardType"],
                            @"type": [formData objectForKey:@"type"],
                            @"site": [formData objectForKey:@"site"],
                            @"site_type": [formData objectForKey:@"site_type"],
                            @"description": [formData objectForKey:@"description"],
                            @"status": @"open",
                            @"action": [formData objectForKey:@"action"]
                            };
    RemoteJson *r = [[RemoteJson alloc] init];
    [sendButtonLabel setEnabled:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        [r addCase:data withImage:UIImagePNGRepresentation([selectedPhoto image])];
        [sendButtonLabel setEnabled:YES];
        [formData removeObjectForKey:@"hazardType"];
        [formData removeObjectForKey:@"type"];
        [formData removeObjectForKey:@"site"];
        [formData removeObjectForKey:@"site_type"];
        [formData removeObjectForKey:@"description"];
        [formData removeObjectForKey:@"action"];
        [chosenCategoryLabel setText:@""];
        [chosenLocation setText:@""];
        [chosenTypeLabel setText:@""];
        
        if (managedObject.managedObjectContext==nil) {
            [self performSegueWithIdentifier:@"backToCaseList" sender:self];
        } else {
            NSManagedObjectContext *context = [self managedObjectContext];
            [context deleteObject:managedObject];
            NSError *error = nil;
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            } else {
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Information"
                                                                      message:@"Hazard report created."
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                [myAlertView show];
                if ([sourceVC isKindOfClass:[CasesTableViewController class] ]) {
                    [self performSegueWithIdentifier:@"backtoHazardList2" sender:self];
                } else {
                    [self performSegueWithIdentifier:@"backToCaseList" sender:self];
                }
            }
        }
    });
}
- (IBAction)unwindToCreateCase:(UIStoryboardSegue *)unwindSegue
{
    if ([unwindSegue.sourceViewController isKindOfClass:[HazardTypesCollectionViewController class]]) {
        HazardTypesCollectionViewController *source = unwindSegue.sourceViewController;
        // if the user clicked Cancel, we don't want to change the color
        if (source.selectedHazardType) {
            hazardType = source.selectedHazardType;
            dispatch_async(dispatch_get_main_queue(), ^{
                //[hazardTypeLabel setText:[NSString stringWithFormat:@"Hazard type %@", hazardType] ];
                chosenCategoryLabel.text = source.selectedHazardType;
                [formData setValue:hazardType forKey:@"hazardType"];
            });
        }
    }
    if ([unwindSegue.sourceViewController isKindOfClass:[SitesTableViewController class]]) {
        SitesTableViewController *source = unwindSegue.sourceViewController;
        // if the user clicked Cancel, we don't want to change the color
        if (source.selectedLocation) {
            site = source.selectedLocation;
            dispatch_async(dispatch_get_main_queue(), ^{
                //[siteLabel setText:[NSString stringWithFormat:@"Location %@", site] ]; //Labelled Location
                chosenLocation.text = source.selectedLocation;
                [formData setValue:site forKey:@"site_type"];
            });
        }
    }
    if ([unwindSegue.sourceViewController isKindOfClass:[PhotoViewController class]]) {
        PhotoViewController *source = unwindSegue.sourceViewController;
        // if the user clicked Cancel, we don't want to change the color
        if (source.selected_photo) {
            image = source.selected_photo;
            dispatch_async(dispatch_get_main_queue(), ^{
                [selectedPhoto setImage:image];
            });
        }
    }
    if ([unwindSegue.sourceViewController isKindOfClass:[TypesTableViewController class]]) {
        TypesTableViewController *source = unwindSegue.sourceViewController;
        // if the user clicked Cancel, we don't want to change the color
        if (source.selectedType) {
            dispatch_async(dispatch_get_main_queue(), ^{
                chosenTypeLabel.text = source.selectedType;
                [formData setValue:source.selectedType forKey:@"type"];
            });
        }
    }
}
@end
