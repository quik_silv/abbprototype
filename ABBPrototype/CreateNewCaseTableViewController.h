//
//  CreateNewCaseTableViewController.h
//  ABBPrototype
//
//  Created by mark on 7/5/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "SZTextView.h"
#import "PhotoViewController.h"
#import "HazardTypesCollectionViewController.h"
#import "SitesTableViewController.h"
#import "CasesTableViewController.h"

@interface CreateNewCaseTableViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate>
@property NSManagedObject *managedObject;
@property NSString *hazardType;
@property NSString *type;
@property NSString *site;
@property NSString *location;
@property NSData *image;
@property NSString *employeeId;
@property NSString *hrDescription;
@property NSString *action;
@property CasesTableViewController *sourceVC;
@property (weak, nonatomic) IBOutlet UILabel *hazardTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *siteLabel;
- (IBAction)saveData:(id)sender;
- (IBAction)submit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *sendButtonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectedPhoto;
@property (weak, nonatomic) IBOutlet UITextField *siteTextField;
@property (weak, nonatomic) IBOutlet SZTextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet SZTextView *actionTextView;

@property (weak, nonatomic) IBOutlet UILabel *chosenTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *chosenCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *chosenLocation;

@end
