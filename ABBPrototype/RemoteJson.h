//
//  RemoteJson.h
//  ABBPrototype
//
//  Created by Mark Wong on 8/1/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#ifndef ABBPrototype_RemoteJson_h
#define ABBPrototype_RemoteJson_h
#import "Foundation/Foundation.h"
@interface RemoteJson: NSObject
- (NSArray *)updateDataFromRemoteSourceFrom:(NSString *)urlString;
- (NSString *) randomStringWithLength: (int) len;
- (void) addAction:(NSString *) actionDescription withId:(NSString *) caseId;
- (void) addCase:(NSDictionary *) data withImage:(NSData *) imageData;
- (void) sendSOT:(NSDictionary *) data withImage:(NSData *) imageData;
- (void) addStopAndCheck:(NSMutableArray *) data withSOT:(NSString *) SOTEmail withEmployeeId:(NSString *) employeeId;
@end
#endif
