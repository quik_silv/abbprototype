//
//  SOTTableViewController.h
//  ABBPrototype
//
//  Created by Mark Wong on 8/31/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"

@interface SOTTableViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *chosenCoInspector;
@property (weak, nonatomic) IBOutlet UILabel *chosenSiteProject;
@property (weak, nonatomic) IBOutlet UITextField *chosenNumberOfPeople;
@property (weak, nonatomic) IBOutlet UILabel *chosenDateTime;
@property (weak, nonatomic) IBOutlet UITextField *chosenDuration;
@property (weak, nonatomic) IBOutlet UILabel *chosenRepresentative;
@property (weak, nonatomic) IBOutlet SZTextView *textWhatAreYouDoing;
@property (weak, nonatomic) IBOutlet SZTextView *textWhatCouldGoWrong;
@property (weak, nonatomic) IBOutlet SZTextView *textWhatAreYourChoices;
@property (weak, nonatomic) IBOutlet SZTextView *textHowCanWorkSafer;
@property (weak, nonatomic) IBOutlet UIImageView *selectedPhoto;



@property (weak, nonatomic) IBOutlet UITextField *chosenAreaObserved;
@property (weak, nonatomic) IBOutlet UITableViewCell *datePickerCell;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (retain) NSDateFormatter *dateFormatter;
- (IBAction)pickerChanged:(UIDatePicker *)sender;
- (void)defaultDate;
@property BOOL datePickerIsShowing;

- (IBAction)saveAndSendButton:(id)sender;

@end
