//
//  SafetyAlertTableViewController.m
//  ABBPrototype
//
//  Created by Mark Wong on 9/6/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "SafetyAlertTableViewController.h"
#import "PDFViewController.h"

@interface SafetyAlertTableViewController ()

@end

@implementation SafetyAlertTableViewController
NSDictionary *documents;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    documents = @{@"2015" : @[
                          @"Lessons_Learned_Briefing_96_-_Serious_Injury_-_Exposed_to_fire_-Brazil_March_2015", @"Lessons_Learned_Briefing_97_-_Serious_Injury_-_Contact_with_electricity_-Sweden_March_2015", @"Lessons_Learned_Briefing_98_-_Serious_Injury_-_Fall_from_height_-_Turkey_March_2015"
                          ]};
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[documents allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSArray *number = [documents objectForKey:[[documents allKeys] objectAtIndex:section]];
    return [number count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[documents allKeys] objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"document" forIndexPath:indexPath];
    
    // Configure the cell...
    //cell.textLabel.text = [documents objectAtIndex:indexPath.row];
    
    NSString *sectionTitle = [[documents allKeys] objectAtIndex:indexPath.section];
    NSArray *sectionItems = [documents objectForKey:sectionTitle];
    cell.textLabel.text = [sectionItems objectAtIndex:indexPath.row];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    if ([segue.identifier isEqualToString:@"selectedDocument"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *sectionTitle = [[documents allKeys] objectAtIndex:indexPath.section];
        NSArray *sectionItems = [documents objectForKey:sectionTitle];
        
        PDFViewController *destViewController = [segue destinationViewController];
        destViewController.fileName = [sectionItems objectAtIndex:indexPath.row];
    }
}


@end
