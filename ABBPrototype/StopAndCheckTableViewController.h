//
//  StopAndCheckTableViewController.h
//  ABBPrototype
//
//  Created by Mark Wong on 8/31/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopAndCheckTableViewController : UITableViewController
- (IBAction)saveAndSendButton:(id)sender;

@end
