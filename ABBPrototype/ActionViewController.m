//
//  ActionViewController.m
//  ABBPrototype
//
//  Created by Mark Wong on 8/18/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "ActionViewController.h"
#import "RemoteJson.h"

@interface ActionViewController ()

@end

@implementation ActionViewController
@synthesize scrollView;
@synthesize hazardTypeText;
@synthesize siteText;
@synthesize caseId;
@synthesize descriptionText;
@synthesize hazardTypeLabel;
@synthesize siteLabel;
@synthesize descriptionLabel;
@synthesize photoImageView;
@synthesize photoUrl;
@synthesize actionDescriptionTextField;
@synthesize actionText;
@synthesize takeActionButtonLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [siteLabel setText:siteText];
    [hazardTypeLabel setText:hazardTypeText];
    [descriptionLabel setText:descriptionText];
    [actionDescriptionTextField setText:actionText];
    
    photoUrl = [photoUrl stringByReplacingOccurrencesOfString:@"media" withString:@"static/media"];
    NSURL *imageUrl = [NSURL URLWithString:photoUrl];
    NSData * image = [NSData dataWithContentsOfURL:imageUrl];
    [photoImageView setImage:[UIImage imageWithData:image] ];
    
    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)takeAction:(id)sender {
    RemoteJson *r = [[RemoteJson alloc] init];
    [takeActionButtonLabel setEnabled:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        [r addAction:actionDescriptionTextField.text withId:caseId];
        [takeActionButtonLabel setEnabled:YES];
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Infomation"
                                                              message:@"Action taken"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    });
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
//    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
//        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
//    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}
@end
