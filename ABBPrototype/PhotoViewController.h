//
//  PhotoViewController.h
//  ABBPrototype
//
//  Created by mark on 7/8/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class PhotoViewController;
//@protocol PhotoViewControllerDelegate <NSObject>
//- (void) addItemViewController:(PhotoViewController *)controller didFinishEnteringItem:(UIImage *)image;
//@end

@interface PhotoViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
//@property (nonatomic, weak) id <PhotoViewControllerDelegate> delegate;
- (IBAction)takePhoto:(id)sender;
- (IBAction)selectPhoto:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *selectedPhoto;
@property UIImage * selected_photo;
@property (weak, nonatomic) NSString * segueSource;
- (IBAction)okButton:(id)sender;

@end
