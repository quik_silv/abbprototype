//
//  CasesTableViewController.m
//  ABBPrototype
//
//  Created by Mark Wong on 8/1/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "AppDelegate.h"
#import "CasesTableViewController.h"
#import "RemoteJson.h"
#import "ActionViewController.h"
#import "CreateNewCaseTableViewController.h"

@interface CasesTableViewController ()

@end

@implementation CasesTableViewController
NSMutableArray *savedData;
NSArray *casesData;
NSArray *sotData;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.

    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HazardReport"];
    savedData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    RemoteJson *r = [[RemoteJson alloc] init];
    casesData = [r updateDataFromRemoteSourceFrom:@"http://python-spotato.rhcloud.com/api/case?format=json"];
    sotData = [r updateDataFromRemoteSourceFrom:@"http://python-spotato.rhcloud.com/api/sot?format=json"];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    if(section == 0) {
        count = [savedData count];
    } else if(section == 1) {
        count = [casesData count];
    } else if(section == 2) {
        count = [sotData count];
    }
    return count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section) {
        case 0:
            sectionName = NSLocalizedString(@"Drafts", @"Drafts");
            break;
        case 1:
            sectionName = NSLocalizedString(@"Hazard Reports", @"Hazard Reports");
            break;
        case 2:
            sectionName = NSLocalizedString(@"Safety Observation Tour", @"Safety Observation Tour");
            break;
            // ...
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    if(indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"caseCellDraft" forIndexPath:indexPath];
        NSManagedObject *saved = [savedData objectAtIndex:indexPath.row];
        [cell.textLabel setText:[NSString stringWithFormat:@"Draft: %@", [saved valueForKey:@"hazard_type"] ] ];
        [cell.detailTextLabel setText:[saved valueForKey:@"site"]];
    } else if(indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"caseCell" forIndexPath:indexPath];
        [cell.textLabel setText:[[casesData objectAtIndex:indexPath.row] objectForKey:@"hazard_type"] ];
        [cell.detailTextLabel setText:[[casesData objectAtIndex:indexPath.row] objectForKey:@"site"] ];
    } else if(indexPath.section == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"caseCellSot" forIndexPath:indexPath];
        [cell.textLabel setText:[[sotData objectAtIndex:indexPath.row] objectForKey:@"siteProject"] ];
        [cell.detailTextLabel setText:[[sotData objectAtIndex:indexPath.row] objectForKey:@"coInspector"] ];
    }
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"selectedCase"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        ActionViewController *destViewController = [segue destinationViewController];
        destViewController.caseId = [[casesData objectAtIndex:indexPath.row] objectForKey:@"id"];
        destViewController.siteText = [[casesData objectAtIndex:indexPath.row] objectForKey:@"site"];
        destViewController.hazardTypeText = [[casesData objectAtIndex:indexPath.row] objectForKey:@"hazard_type"];
        destViewController.descriptionText = [[casesData objectAtIndex:indexPath.row] objectForKey:@"description"];
        destViewController.photoUrl = [[casesData objectAtIndex:indexPath.row] objectForKey:@"image"];
        destViewController.actionText = [[casesData objectAtIndex:indexPath.row] objectForKey:@"action"];
    } else if([segue.identifier isEqualToString:@"draftHazard"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *saved = [savedData objectAtIndex:indexPath.row];
        
        CreateNewCaseTableViewController *destViewController = [segue destinationViewController];
        destViewController.managedObject = saved;
        destViewController.employeeId = [saved valueForKey:@"employee_id"];
        destViewController.hazardType = [saved valueForKey:@"hazard_type"];
        destViewController.type = [saved valueForKey:@"type"];
        destViewController.location = [saved valueForKey:@"site_type"];
        destViewController.site = [saved valueForKey:@"site"];
        destViewController.image = [saved valueForKey:@"image"]; //NSData
        destViewController.hrDescription = [saved valueForKey:@"hrDescription"];
        destViewController.action = [saved valueForKey:@"action"];
        destViewController.sourceVC = self;
    }
    
}


- (IBAction)casesRefresh:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        RemoteJson *r = [[RemoteJson alloc] init];
        casesData = [r updateDataFromRemoteSourceFrom:@"http://python-spotato.rhcloud.com/api/case?format=json"];
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    });
}
- (IBAction)unwindToHazardList:(UIStoryboardSegue *)unwindSegue {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HazardReport"];
        savedData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    });
}
@end
