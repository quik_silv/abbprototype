//
//  SOTTableViewController.m
//  ABBPrototype
//
//  Created by Mark Wong on 8/31/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "SOTTableViewController.h"
#import "PhotoViewController.h"
#import "CoInspectorTableViewController.h"
#import "SitesTableViewController.h"
#import "RemoteJson.h"

@interface SOTTableViewController ()

@end

@implementation SOTTableViewController

@synthesize chosenCoInspector;
@synthesize chosenSiteProject;
@synthesize chosenDateTime;
@synthesize chosenAreaObserved;
@synthesize chosenDuration;
@synthesize chosenNumberOfPeople;
@synthesize chosenRepresentative;
@synthesize selectedPhoto;

@synthesize textWhatAreYouDoing;
@synthesize textWhatCouldGoWrong;
@synthesize textWhatAreYourChoices;
@synthesize textHowCanWorkSafer;

@synthesize datePickerIsShowing;
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self defaultDate];
    [chosenAreaObserved setDelegate:self];
    [chosenDuration setDelegate:self];
    [chosenNumberOfPeople setDelegate:self];
    
    [textWhatAreYouDoing setDelegate:self];
    [textWhatCouldGoWrong setDelegate:self];
    [textWhatAreYourChoices setDelegate:self];
    [textHowCanWorkSafer setDelegate:self];
}
- (void)defaultDate {
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    NSDate *defaultDate = [NSDate date];
    [chosenDateTime setText:[self.dateFormatter stringFromDate:defaultDate] ];
}
#define kDatePickerCellHeight 164
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = self.tableView.rowHeight;
    if (indexPath.row == 5){
        height = self.datePickerIsShowing ? kDatePickerCellHeight : 0.0f;
    }
    if (indexPath.row > 7 && indexPath.row < 12) {
        height = 105;
    }
    if (indexPath.row == 12) {
        height = 75;
    }
    if (indexPath.row == 13) {
        height = 75;
    }
    return height;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4){
        if (self.datePickerIsShowing){
            [self hideDatePickerCell];
        }else {
            [self showDatePickerCell];
        }
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)showDatePickerCell {
    self.datePickerIsShowing = YES;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    self.datePicker.hidden = NO;
    self.datePicker.alpha = 0.0f;
    [UIView animateWithDuration:0.25 animations:^{
        self.datePicker.alpha = 1.0f;
    }];
}

- (void)hideDatePickerCell {
    self.datePickerIsShowing = NO;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.datePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.datePicker.hidden = YES;
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"" forIndexPath:indexPath];

    return cell;
}
*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"go_to_site"]) {
        SitesTableViewController *destViewController = [segue destinationViewController];
        destViewController.segueSource = @"SOT";
    }
    if ([segue.identifier isEqualToString:@"SOTtoPhoto"]) {
        PhotoViewController *destViewController = [segue destinationViewController];
        destViewController.segueSource = @"SOT";
    }
}
- (IBAction)unwindToSOTForm:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[CoInspectorTableViewController class]]) {
        CoInspectorTableViewController *source = unwindSegue.sourceViewController;
        // if the user clicked Cancel, we don't want to change the color
        if (source.selectedCoInspector) {
            dispatch_async(dispatch_get_main_queue(), ^{
                chosenCoInspector.text = source.selectedCoInspector;
            });
        }
    }
    if ([unwindSegue.sourceViewController isKindOfClass:[SitesTableViewController class]]) {
        SitesTableViewController *source = unwindSegue.sourceViewController;
        // if the user clicked Cancel, we don't want to change the color
        if (source.selectedLocation) {
            dispatch_async(dispatch_get_main_queue(), ^{
                chosenSiteProject.text = source.selectedLocation;
            });
        }
    }
    if ([unwindSegue.sourceViewController isKindOfClass:[PhotoViewController class]]) {
        PhotoViewController *source = unwindSegue.sourceViewController;
        // if the user clicked Cancel, we don't want to change the color
        if (source.selected_photo) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [selectedPhoto setImage:source.selected_photo];
            });
        }
    }
}

- (IBAction)pickerChanged:(UIDatePicker *)sender {
    [chosenDateTime setText:[self.dateFormatter stringFromDate:sender.date] ];
}

- (IBAction)saveAndSendButton:(id)sender {
    NSDictionary * data = @{
                            @"coInspector": chosenCoInspector.text,
                            @"siteProject": chosenSiteProject.text,
                            @"numberOfPeople": chosenNumberOfPeople.text,
                            @"dateTime": chosenDateTime.text,
                            @"duration": chosenDuration.text,
                            @"representative": chosenRepresentative.text,
                            @"whatAreYouDoing": textWhatAreYouDoing.text,
                            @"whatCouldGoWrong": textWhatCouldGoWrong.text,
                            @"whatAreYourChoices": textWhatAreYourChoices.text,
                            @"howCanWorkSafer":textHowCanWorkSafer.text
                            };
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        [r sendSOT:data withImage:UIImagePNGRepresentation([selectedPhoto image]) ];
        [self performSegueWithIdentifier:@"SOTtoHome" sender:self];
    });
}
@end
