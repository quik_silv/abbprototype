//
//  HazardReport.h
//  ABBPrototype
//
//  Created by Mark Wong on 9/14/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface HazardReport : NSManagedObject

@property (nonatomic, retain) NSString * employee_id;
@property (nonatomic, retain) NSString * hazard_type;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * site;
@property (nonatomic, retain) NSString * site_type;
@property (nonatomic, retain) NSString * hrDescription;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSString * created;
@property (nonatomic, retain) NSData * image;

@end
