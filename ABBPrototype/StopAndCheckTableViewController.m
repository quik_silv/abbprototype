//
//  StopAndCheckTableViewController.m
//  ABBPrototype
//
//  Created by Mark Wong on 8/31/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "StopAndCheckTableViewController.h"
#import "YesNoTableViewCell.h"
#import "RemoteJson.h"

@interface StopAndCheckTableViewController ()

@end

@implementation StopAndCheckTableViewController

NSDictionary *stopAndCheckItems;
NSMutableArray *answers;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    answers = [[NSMutableArray alloc] init];
    stopAndCheckItems = @{@"General" : @[@"Have you received the site safety induction?",
                                         @"Have you been briefed on the risk assessment and are aware of the risks involved in this job?",
                                         @"Are you trained adequately to perform this job safely?"],
                          @"Electrical" : @[@"Is Licenced Electrical Worker (LEW) required to carry out the work?",
                                            @"Are you exposed to live voltages?",
                                            @"Are live equipment clearly identified, cordoned off and provided with warning signs?",
                                            @"have you isolated the electrical system with a locak and tag?",
                                            @"Have you confirmed that the electrical system is dead?",
                                            @"Have you discharged the residual charges with proper discharge sticks?"],
                          @"Overall assessment" : @[@"Is it safe to work after implementing the control measures?", @"placeholder for button"],
                          };
    //identical
    answers = [[NSMutableArray alloc] initWithObjects:@"Have you received the site safety induction? NO",
               @"Have you been briefed on the risk assessment and are aware of the risks involved in this job? NO",
               @"Are you trained adequately to perform this job safely? NO", @"Is Licenced Electrical Worker (LEW) required to carry out the work? NO",
               @"Are you exposed to live voltages? NO",
               @"Are live equipment clearly identified, cordoned off and provided with warning signs? NO",
               @"have you isolated the electrical system with a locak and tag? NO",
               @"Have you confirmed that the electrical system is dead? NO",
               @"Have you discharged the residual charges with proper discharge sticks? NO",
               @"Is it safe to work after implementing the control measures? NO", @"", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[stopAndCheckItems allKeys] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[stopAndCheckItems allKeys] objectAtIndex:section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSArray *number = [stopAndCheckItems objectForKey:[[stopAndCheckItems allKeys] objectAtIndex:section]];
    return [number count];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 84;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"stopAndCheckCell" forIndexPath:indexPath];
    
    if (indexPath.section == 2 && indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"buttonCell" forIndexPath:indexPath];
        return cell;
    } else {
        YesNoTableViewCell *cell = (YesNoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"stopAndCheckCell"];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"YesNoUITableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        [cell setDelegate:self];
        
        NSString *sectionTitle = [[stopAndCheckItems allKeys] objectAtIndex:indexPath.section];
        NSArray *sectionItems = [stopAndCheckItems objectForKey:sectionTitle];
        cell.customLabel.text = [sectionItems objectAtIndex:indexPath.row];
        return cell;
    }
}
- (void)answeredItems:(YesNoTableViewCell *)cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSInteger modifiedRow = indexPath.section % 2 == 0 ? indexPath.row : indexPath.row+3;
    if (cell.yesNoSegmentSelection.selectedSegmentIndex == 1) {
        [answers replaceObjectAtIndex:modifiedRow withObject:[NSString stringWithFormat:@"%@ YES", cell.customLabel.text] ];
    } else {
        [answers replaceObjectAtIndex:modifiedRow withObject:[NSString stringWithFormat:@"%@ NO", cell.customLabel.text] ];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveAndSendButton:(id)sender {
    RemoteJson *r = [[RemoteJson alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        [r addStopAndCheck:answers withSOT:@"mark+999@squarepotato.com" withEmployeeId:@"123456"];
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Information"
                                                              message:@"Stop and Check Report sent"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
        [self performSegueWithIdentifier:@"SnCtoHome" sender:self];
    });
}
@end
