//
//  TrainingsTableViewController.m
//  ABBPrototype
//
//  Created by Mark Wong on 9/17/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "TrainingsTableViewController.h"
#import "RemoteJson.h"

@interface TrainingsTableViewController ()

@end

@implementation TrainingsTableViewController
NSArray *allData;
NSArray *trainingsData;
NSMutableArray *m_trainingsData;
NSArray *searchResults;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    RemoteJson *r = [[RemoteJson alloc] init];
    allData = [r updateDataFromRemoteSourceFrom:@"http://python-spotato.rhcloud.com/api/training?format=json"];
    m_trainingsData = [[NSMutableArray alloc] init];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"employeeId matches[c] %@", @"111111"];
    trainingsData = [allData filteredArrayUsingPredicate:resultPredicate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
        
    } else {
        return [trainingsData count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"trainingCell"];

    if (tableView == self.searchDisplayController.searchResultsTableView) {
        [cell.textLabel setText:[[searchResults objectAtIndex:indexPath.row] objectForKey:@"email"] ];
        [cell.detailTextLabel setText:[[searchResults objectAtIndex:indexPath.row] objectForKey:@"employeeId"] ];
    } else {
        [cell.textLabel setText:[[trainingsData objectAtIndex:indexPath.row] objectForKey:@"training_list"] ];
        [cell.detailTextLabel setText:[NSString stringWithFormat:@"Expiry: %@", [[trainingsData objectAtIndex:indexPath.row] objectForKey:@"expiryDate"] ] ];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        NSString * email = [[searchResults objectAtIndex:indexPath.row] objectForKey:@"email"];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"email contains[cd] %@", email];
        [m_trainingsData removeAllObjects];
        [m_trainingsData addObjectsFromArray:[allData filteredArrayUsingPredicate:resultPredicate] ];
        trainingsData = [m_trainingsData mutableCopy];
        [self.tableView reloadData];
        self.navigationItem.title = [NSString stringWithFormat:@"%@", email];
        [self.searchDisplayController setActive:NO];
        [self.searchDisplayController.searchBar resignFirstResponder];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"email contains[cd] %@", searchText];
    searchResults = [allData filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    return YES;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
