//
//  PDFViewController.h
//  ABBPrototype
//
//  Created by Mark Wong on 9/6/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDFViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *pdfViewer;
@property NSString *fileName;
@end
