//
//  HazardTypesCollectionViewController.h
//  ABBPrototype
//
//  Created by Mark Wong on 7/4/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class HazardTypesCollectionViewController;
//@protocol HazardTypesCollectionViewControllerDelegate <NSObject>
//- (void) addHazardTypeViewController:(HazardTypesCollectionViewController *)controller didFinishEnteringItem:(NSString *)type;
//@end
@interface HazardTypesCollectionViewController : UICollectionViewController
//@property (nonatomic, weak) id <HazardTypesCollectionViewControllerDelegate> delegate;
@property NSString * selectedHazardType;
@end
