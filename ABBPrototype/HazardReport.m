//
//  HazardReport.m
//  ABBPrototype
//
//  Created by Mark Wong on 9/14/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "HazardReport.h"


@implementation HazardReport

@dynamic employee_id;
@dynamic hazard_type;
@dynamic type;
@dynamic site;
@dynamic site_type;
@dynamic hrDescription;
@dynamic status;
@dynamic action;
@dynamic created;
@dynamic image;

@end
