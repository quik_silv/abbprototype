//
//  HazardTypesCollectionViewController.m
//  ABBPrototype
//
//  Created by Mark Wong on 7/4/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "HazardTypesCollectionViewController.h"
#import "CreateNewCaseTableViewController.h"

@interface HazardTypesCollectionViewController ()
{
    NSArray *hazardTypesImages;
    NSArray *hazardTypeText;
}
@end

@implementation HazardTypesCollectionViewController

static NSString * const reuseIdentifier = @"hazard_type";
//@synthesize delegate;
@synthesize selectedHazardType;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    hazardTypesImages = [NSArray arrayWithObjects:@"slipsntrips.png", @"housekeeping.png", @"movingmachinery.png", @"struckbyobject.png", @"workatheight.png", @"fallingobject.png", @"electricalhazard.png", @"lackofppe.png", @"others.png", nil];
    hazardTypeText = [NSArray arrayWithObjects:@"Slips and trips", @"House keeping", @"Moving Machinery", @"Struck by object", @"Work at height", @"Falling Object", @"Electrical hazard", @"Lack of PPE", @"Others", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [hazardTypesImages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    UIImageView *hazardImageView = (UIImageView *)[cell viewWithTag:100];
    hazardImageView.image = [UIImage imageNamed:[hazardTypesImages objectAtIndex:indexPath.row]];
    UILabel *hazardLabel = (UILabel *)[cell viewWithTag:101];
    hazardLabel.text = [hazardTypeText objectAtIndex:indexPath.row];
    return cell;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //if ([segue.identifier isEqualToString:@"selected_hazard_type"]) {
        NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
//        CreateNewCaseTableViewController *destViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
//        destViewController.hazardType = [NSString stringWithFormat:@"%@", [hazardTypeText objectAtIndex:indexPath.row] ];
        
//        self.delegate = [segue destinationViewController];
//        [self.delegate addHazardTypeViewController:self didFinishEnteringItem:[hazardTypeText objectAtIndex:indexPath.row] ];
    //}
//    NSLog(@"%@", self.navigationController.viewControllers);
//    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
//    [allViewControllers removeObjectAtIndex:[allViewControllers count] - 1];
//    NSLog(@"%@", allViewControllers);
//    [self.navigationController setViewControllers:allViewControllers animated:NO];
    
//    UIViewController *prevVC = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2];
//    [self.navigationController popToViewController:prevVC animated:NO];
    
    //[self.navigationController popViewControllerAnimated:YES];
}
#pragma mark <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // always reload the selected cell, so we will add the border to that cell
    selectedHazardType = [hazardTypeText objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"selected_hazard_type" sender:self];
}
/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/
@end
