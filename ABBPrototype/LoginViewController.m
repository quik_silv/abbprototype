//
//  LoginViewController.m
//  ABBPrototype
//
//  Created by mark on 7/8/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "LoginViewController.h"
#import "KeychainItemWrapper.h"
#import "CreateNewCaseTableViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize employeeIdField;
@synthesize passwordField;
@synthesize loginButton;
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"YourAppLogin" accessGroup:nil];
    NSString * password = [keychainItem objectForKey:(__bridge id)kSecValueData];
    NSString * username = [keychainItem objectForKey:(__bridge id)kSecAttrAccount];
    //&& [password isEqualToString:@"123456"])
    if (![username isEqualToString:@""]) {
        [self performSegueWithIdentifier:@"login" sender:self];
    }
    [employeeIdField setDelegate:self];
    [passwordField setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginAction:(id)sender {
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"YourAppLogin" accessGroup:nil];
//    NSString * password = [keychainItem objectForKey:(__bridge id)kSecValueData];
//    NSString * username = [keychainItem objectForKey:(__bridge id)kSecAttrAccount];
    if ([employeeIdField.text isEqual: @"123456"] && [passwordField.text isEqual: @"123456"]) {
        [keychainItem setObject:passwordField.text forKey:(__bridge id)kSecValueData];
        [keychainItem setObject:employeeIdField.text forKey:(__bridge id)kSecAttrAccount];
        //[NSThread sleepForTimeInterval:2.0f];
        [self performSegueWithIdentifier:@"login" sender:sender];
    } else {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Wrond username or password."
                                                              message:@"Please contact HSE Dorothy Tan."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    //[keychainItem resetKeychainItem];
}
- (IBAction)unwindToLogin:(UIStoryboardSegue *)unwindSegue
{ }
@end
