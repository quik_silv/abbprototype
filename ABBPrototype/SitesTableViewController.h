//
//  SitesTableViewController.h
//  ABBPrototype
//
//  Created by mark on 7/7/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class SitesTableViewController;
//@protocol SitesTableViewControllerDelegate <NSObject>
//
//- (void) addLocationTableViewController: (SitesTableViewController *)controller didFinishEnteringItem:(NSString *) type;
//@end
@interface SitesTableViewController : UITableViewController
//@property (nonatomic, weak) id <SitesTableViewControllerDelegate> delegate;
@property NSString * selectedLocation;
@property (weak, nonatomic) NSString * segueSource;
@end
