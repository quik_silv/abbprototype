//
//  YesNoTableViewCell.m
//  ABBPrototype
//
//  Created by Mark Wong on 8/31/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "YesNoTableViewCell.h"

@implementation YesNoTableViewCell

@synthesize customLabel;
@synthesize delegate;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id) initWithArgument:(id)someArgument reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // custom setup
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.delegate = nil;
}
- (IBAction)yesNoSegment:(id)sender {
    if ([self.delegate respondsToSelector:@selector(answeredItems:)])
        [self.delegate answeredItems:self];
}
@end
