//
//  PhotoViewController.m
//  ABBPrototype
//
//  Created by mark on 7/8/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import "PhotoViewController.h"

@interface PhotoViewController ()

@end

@implementation PhotoViewController
@synthesize selectedPhoto;
@synthesize selected_photo; //pass this variable to sourceController, SOT or HazardReport
@synthesize segueSource;
//@synthesize delegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tabBarController.tabBar.hidden = YES;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    self.delegate = [segue destinationViewController];
//    [self.delegate addItemViewController:self didFinishEnteringItem:[selectedPhoto image] ];
}


- (IBAction)takePhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:NO completion:NULL];
}

- (IBAction)selectPhoto:(UIButton *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:NO completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [selectedPhoto setImage:chosenImage];
    UIImageWriteToSavedPhotosAlbum(chosenImage,
                                   self,
                                   nil,
                                   nil);
    selected_photo = chosenImage;
    [picker dismissViewControllerAnimated:NO completion:NULL];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:NO completion:NULL];
    
}
- (IBAction)okButton:(id)sender {
    if([segueSource isEqual: @"SOT"]) {
        [self performSegueWithIdentifier:@"SOTmodalPhoto" sender:self];
    } else {
        [self performSegueWithIdentifier:@"HRmodalPhoto" sender:self];
    }
}
@end
