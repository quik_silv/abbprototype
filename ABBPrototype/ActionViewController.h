//
//  ActionViewController.h
//  ABBPrototype
//
//  Created by Mark Wong on 8/18/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *hazardTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *siteLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) NSString *hazardTypeText;
@property (weak, nonatomic) NSString *siteText;
@property (weak, nonatomic) NSString *descriptionText;
@property (weak, nonatomic) NSString *actionText;
@property (weak, nonatomic) NSString *caseId;
@property (weak, nonatomic) NSString *photoUrl;
@property (weak, nonatomic) IBOutlet UITextView *actionDescriptionTextField;
- (IBAction)takeAction:(id)sender;
- (void)registerForKeyboardNotifications;
- (void)keyboardWasShown:(NSNotification*)aNotification;
- (void)keyboardWillBeHidden:(NSNotification*)aNotification;
@property (weak, nonatomic) IBOutlet UIButton *takeActionButtonLabel;

@end
