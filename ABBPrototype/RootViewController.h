//
//  RootViewController.h
//  ABBPrototype
//
//  Created by Mark Wong on 7/4/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

