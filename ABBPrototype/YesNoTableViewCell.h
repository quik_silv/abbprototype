//
//  YesNoTableViewCell.h
//  ABBPrototype
//
//  Created by Mark Wong on 8/31/15.
//  Copyright (c) 2015 Square Potato. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol YesNoTableViewCellDelegate;
@interface YesNoTableViewCell : UITableViewCell
@property (assign, nonatomic) id <YesNoTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *customLabel;
- (IBAction)yesNoSegment:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *yesNoSegmentSelection;


- (id) initWithArgument:(id)someArgument reuseIdentifier:(NSString *)reuseIdentifier;
@end

@protocol YesNoTableViewCellDelegate <NSObject>

@optional

- (void)answeredItems:(YesNoTableViewCell *)cell;
@end